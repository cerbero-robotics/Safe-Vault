#include <LiquidCrystal.h>
// https://docs.arduino.cc/learn/electronics/lcd-displays
const int rs = 13, en = 12, d4 = 11, d5 = 10, d6 = 9, d7 = 8;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);