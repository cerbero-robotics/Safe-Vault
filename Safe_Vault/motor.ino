void turn(unsigned int x) {
  digitalWrite(MotorPins[x % 4], HIGH);
  digitalWrite(MotorPins[(x + 1) % 4], LOW);
  digitalWrite(MotorPins[(x + 2) % 4], LOW);
  digitalWrite(MotorPins[(x + 3) % 4], LOW);
}
/*
Motor can only turn < 16 times
we use 11 bits because we need to multiply by 2048
leaves us with 5 bits to play with, 2^5 -1 = 31
if rotations = 32, we will overflow and go back to 0
*/
void motor_clockwise(unsigned int rotations = 1) {
  //safety check
  if (rotations > 31) {
    //unsafe, might run out of memory
    motor_clockwise(rotations - 31);
    rotations = 31;
  }
  //mutiply rotations by 2048 = 2^11, because ~2000 iterations per loop
  rotations = rotations << 10;
  for (int x = 0; x <= rotations; x++) {
    turn(x);
    delay(2);
  }
}

void motor_anticlockwise(unsigned int rotations = 1) {
  //safety check
  if (rotations > 31) {
    //unsafe, might run out of memory
    motor_anticlockwise(rotations - 31);
    rotations = 31;
  }
  //mutiply rotations by 2048 = 2^11, because ~2000 iterations per loop
  rotations = rotations << 11;
  for (int x = rotations; x >= 0; x--) {
    turn(x);
    delay(2);
  }
}
