#define OPEN 1
#define CLOSED 0

void green_light() {
  digitalWrite(1, HIGH);
  delay(1000);
  digitalWrite(1, LOW);
}

void red_light() {
  digitalWrite(2, HIGH);
  delay(1000);
  digitalWrite(2, LOW);
}
void setup() {
  //setup Motor pins
  for (int x = 0; x <= 3; x++) {
    pinMode(MotorPins[x], OUTPUT);
  }
  // lights
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  // set up the LCD's number of columns and rows:
  lcd.begin(0, 1);
}

const String password = "1111";
String keypad_input;

// closed 0, open 1
int safe_mode = CLOSED;

void loop() {
  char key = keypad.getKey();
  switch (safe_mode) {
    case OPEN:
      if (key == '#') {
        motor_clockwise();
        safe_mode = CLOSED;
      }
      break;
    case CLOSED:
      lcd.setCursor(0, 1);
      lcd.print(keypad_input);
      if (key) {
        switch (key) {
          case '*':
            //clear
            keypad_input = "";
            lcd.clear();
            break;
          case '#':
            //enter
            if (keypad_input == password) {
              //correct password
              green_light();
              motor_anticlockwise();              
              safe_mode = OPEN;
            } else {
              //wrong password
              red_light();
              lcd.setCursor(0, 1);
              lcd.print("WRONG!");
              delay(2000);
              lcd.clear();
            }
            break;
          default:
            //type number
            keypad_input += key;
            break;
        }
      }
      break;
  }
}