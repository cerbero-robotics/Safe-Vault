//keyboard pins
const int ROW_NUM = 4;
const int COLUMN_NUM = 3;

byte pin_rows[ROW_NUM] = {22, 24, 26, 28};
byte pin_column[COLUMN_NUM] = {23, 25, 27};
// 0 1 6 7 8 9 10
//motor pins
int MotorPins[4] = {47, 49, 51, 53};