# Safe Vault

ISM Cerbero Robotics club

---

Designed for use with arduino Mega

| Pins | Connection |
| :-:  |     :-:    |
|      |     LCD    |
|  8   |     d7     |
|  9   |     d6     |
|  10  |     d5     |
|  11  |     d4     |
|  12  |     en     |
|  13  |     rs     |
|      |    Motor   |
|  47  |            |
|  49  |            |
|  51  |            |
|  53  |            |
|      |   KeyPad   |
|  22  |    row 1   |
|  23  |  column 1  |
|  24  |    row 2   |
|  25  |  column 2  |
|  26  |    row 3   |
|  27  |  column 3  |
|  28  |    row 4   |
|      |  Lights    |
|  1   |   green    |
|  2   |    red     |

---

```mermaid
graph TD
	Pin-->|correct|correct[lights green]
	Pin-->|wrong| wrong[lights red]
	correct-->motor_open[motor: open door]
	wrong-->Pin
	motor_open-.->|close button|motor_close[motor: close door]
	motor_close-->Pin
	Pin o-.-o screen
```
